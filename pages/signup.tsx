import { NextPage } from 'next'
import React from 'react'
import { SignupTemplate } from '../components/templates'

const Signup: NextPage = () => <SignupTemplate />

export default Signup
