import React from 'react'
import { NextPage } from 'next'

import QuestionsContextProvider from '../context/Questions'
import { QuestionsTemplate } from '../components/templates'

const Questions: NextPage = () => (
  <QuestionsContextProvider>
    <QuestionsTemplate />
  </QuestionsContextProvider>
)

export default Questions
