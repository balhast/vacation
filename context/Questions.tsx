import React from 'react'

interface IQuestionsContext {
  question: number
  values: {
    1: {
      sightseeing: boolean
      bigCities: boolean
      beachResorts: boolean
      waterActivities: boolean
      restaurants: boolean
      parties: boolean
    }
  }
  nextQuestion: () => void
  prevQuestion: () => void
  setValuesForQuestion: (
    values: Record<string, string | number | boolean>,
    question: number
  ) => void
}

export const QuestionsContext = React.createContext<IQuestionsContext>(
  {} as IQuestionsContext
)

const initialValues: IQuestionsContext['values'] = {
  1: {
    sightseeing: false,
    bigCities: false,
    beachResorts: false,
    waterActivities: false,
    restaurants: false,
    parties: false,
  },
}

const QuestionsContextProvider: React.FC = ({ children }) => {
  const [question, setQuestion] = React.useState(1)
  const [values, setValues] =
    React.useState<IQuestionsContext['values']>(initialValues)

  const nextQuestion = React.useCallback(() => {
    setQuestion(question + 1)
  }, [question])

  const prevQuestion = React.useCallback(() => {
    setQuestion(question - 1)
  }, [question])

  const setValuesForQuestion = React.useCallback(
    (valuesForQuestion: Record<string, any>, question: number) => {
      setValues({
        ...values,
        [question]: valuesForQuestion,
      })
    },
    [values]
  )

  return (
    <QuestionsContext.Provider
      value={{
        question,
        nextQuestion,
        prevQuestion,
        values,
        setValuesForQuestion,
      }}
    >
      {children}
    </QuestionsContext.Provider>
  )
}

export default QuestionsContextProvider
