import React from 'react'
import { Questions } from '../../organisms'

const QuestsionsTemplate: React.FC = () => {
  return (
    <div className="h-auto flex justify-center">
      <Questions />
    </div>
  )
}

export default QuestsionsTemplate
