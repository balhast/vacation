import React from 'react'
import { Home } from '../../organisms'

const HomeTemplate: React.FC = () => {
  return <Home />
}

export default HomeTemplate
