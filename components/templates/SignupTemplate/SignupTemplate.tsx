import React from 'react'
import { SignupCard } from '../../organisms'

const SignupTemplate: React.FC = () => {
  return (
    <div className="h-screen flex justify-center items-center">
      <SignupCard />
    </div>
  )
}

export default SignupTemplate
