import React from 'react'
import { SignupForm } from '../../molecules'

const SignupCard: React.FC = () => {
  return (
    <div className="border border-solid border-gray-100 rounded-xl shadow-lg pb-6 bg-white">
      <div className="flex flex-col py-3 mb-6 border-b border-gray-100">
        <h1 className="flex justify-center text-3xl text-gray-500 font-semibold mb-2">
          Let's get started
        </h1>
        <p className="flex justify-center text-xs text-gray-300">
          You're steps away from your dream destination
        </p>
      </div>
      <SignupForm />
    </div>
  )
}

export default SignupCard
