import React from 'react'

import { Navbar, Question1, Question2 } from '../../molecules'

type Props = {
  question: number
}

const Questions: React.FC<Props> = ({ question }) => {
  return (
    <div className="flex flex-col max-w-5xl">
      <Navbar />
      {question === 1 && <Question1 />}
      {question === 2 && <Question2 />}
    </div>
  )
}

export default Questions
