import React from 'react'

import { QuestionsContext } from '../../../context'

import Questions from './Questions'

const QuestionsContainer: React.FC = () => {
  const { question } = React.useContext(QuestionsContext)

  return <Questions question={question} />
}

export default QuestionsContainer
