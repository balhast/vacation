import React from 'react'
import Home from './Home'

type Props = React.HTMLProps<HTMLDivElement>

const HomeContainer: React.FC<Props> = (props) => {
  return <Home {...props} />
}

export default HomeContainer
