import React from 'react'

const Home: React.FC = () => {
  return (
    <div className="h-screen flex justify-center bg-homepage bg-cover bg-top">
      <div className="flex items-center">
        <div className="flex flex-col">
          <h1 className="text-white text-2xl mb-6">
            Ready for a new adventure? Plan your next trip here. <br /> Sign up
            to get started!
          </h1>
          <a
            className="w-36 py-3 px-10 rounded-lg border border-purple-700 bg-purple-700 text-white hover:bg-purple-800 cursor-pointer"
            href="/signup"
          >
            Sign up
          </a>
        </div>
      </div>
    </div>
  )
}

export default Home
