import classNames from 'classnames'
import React from 'react'

type Props = React.HTMLProps<HTMLAnchorElement> & {
  caption: string
}

const NavLink: React.FC<Props> = ({ className, caption, ...rest }) => {
  return (
    <a
      {...rest}
      className={classNames(
        'text-gray-600 py-4 px-6 mx-2 border-b hover:border-purple-700 hover:text-purple-700 cursor-pointer',
        className
      )}
    >
      {caption}
    </a>
  )
}

export default NavLink
