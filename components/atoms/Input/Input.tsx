import React from 'react'
import classNames from 'classnames'

type Props = React.HTMLProps<HTMLInputElement>

const Input: React.FC<Props> = ({ className, ...rest }) => {
  return (
    <input
      {...rest}
      className={classNames(
        'p-3 placeholder-gray-400 text-sm border border-gray-200 rounded-lg focus:outline-none focus:ring-2 focus:ring-purple-700',
        className
      )}
    />
  )
}

export default Input
