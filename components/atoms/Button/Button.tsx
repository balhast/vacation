import classNames from 'classnames'
import React from 'react'

type ButtonVariant = 'default' | 'outlined'

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  caption: string
  variant?: ButtonVariant
}

const Button: React.FC<Props> = ({ className, caption, variant, ...rest }) => {
  return (
    <button
      {...rest}
      className={classNames(
        'py-3 px-10 rounded-lg border border-purple-700',
        {
          'bg-purple-700 text-white hover:bg-purple-800': variant === 'default',
          'bg-white text-purple-700 hover:bg-purple-700 hover:text-white':
            variant === 'outlined',
        },
        className
      )}
    >
      {caption}
    </button>
  )
}

export default Button
