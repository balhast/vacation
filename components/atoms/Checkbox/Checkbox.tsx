import classNames from 'classnames'
import React from 'react'

type Props = React.HTMLProps<HTMLInputElement>

const Checkbox: React.FC<Props> = ({ className, ...rest }) => {
  return (
    <input
      {...rest}
      className={classNames(
        'text-purple-700 focus:ring-offset-0 focus:ring-0 flex self-center mx-1 border-gray-400',
        className
      )}
      type="checkbox"
    />
  )
}

export default Checkbox
