import React from 'react'
import { NavLink } from '../../atoms'

const Navbar: React.FC = () => {
  return (
    <nav className="mt-10 mb-16">
      <NavLink caption="what" />
      <NavLink caption="where" />
      <NavLink caption="when" />
      <NavLink caption="who" />
    </nav>
  )
}

export default Navbar
