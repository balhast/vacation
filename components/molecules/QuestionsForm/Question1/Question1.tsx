import React from 'react'
import { Grid } from '@mui/material'
import classNames from 'classnames'

import { Button, Checkbox } from '../../../atoms'

const items = [
  {
    name: 'sightseeing',
    classPostfix: 'sightseeing',
    label: 'Sightseeing',
  },
  {
    name: 'bigCities',
    classPostfix: 'big-cities',
    label: 'Big cities',
  },
  {
    name: 'beachResorts',
    classPostfix: 'beach-resorts',
    label: 'Beach resorts',
  },
  {
    name: 'waterActivities',
    classPostfix: 'water-activities',
    label: 'Water activities',
  },
  {
    name: 'restaurants',
    classPostfix: 'restaurants',
    label: 'Restaurants',
  },
  {
    name: 'parties',
    classPostfix: 'parties',
    label: 'Parties',
  },
]

type Props = React.HTMLProps<HTMLFormElement> & {
  handleChange: (e: React.FormEvent<HTMLInputElement>) => void
  nextQuestion: () => void
  values: Record<string, boolean>
}

const Question1: React.FC<Props> = ({
  handleChange,
  className,
  values,
  ...rest
}) => {
  return (
    <form {...rest}>
      <Grid container>
        <Grid item xs={10}>
          <h1 className="text-3xl text-gray-700 py-4 mb-4">
            What would you like to do?
          </h1>
        </Grid>
        <Grid item xs={2}>
          <Button
            className="mt-2"
            type="submit"
            caption="Next"
            variant="outlined"
          />
        </Grid>
        {items.map(({ name, classPostfix, label }) => (
          <Grid key={name} item xs={4}>
            <div
              className={classNames('bg-cover bg-center', {
                [`bg-${classPostfix}`]: !values[name],
                'bg-purple-700': values[name],
              })}
            >
              <label className="text-white text-lg py-36 flex justify-center hover:bg-black hover:bg-opacity-25 cursor-pointer">
                <Checkbox
                  className="w-0 opacity-0"
                  name={name}
                  onChange={handleChange}
                />
                {label}
              </label>
            </div>
          </Grid>
        ))}
      </Grid>
    </form>
  )
}

export default Question1
