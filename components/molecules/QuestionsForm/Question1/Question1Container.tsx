import React from 'react'
import { useFormik } from 'formik'

import { QuestionsContext } from '../../../../context'
import Question1 from './Question1'

type Props = React.HTMLProps<HTMLFormElement>

const Question1Container: React.FC<Props> = (props) => {
  const { nextQuestion, setValuesForQuestion } =
    React.useContext(QuestionsContext)

  const formik = useFormik({
    initialValues: {
      sightseeing: false,
      bigCities: false,
      beachResorts: false,
      waterActivities: false,
      restaurants: false,
      parties: false,
    },
    onSubmit: ({
      sightseeing,
      bigCities,
      beachResorts,
      waterActivities,
      restaurants,
      parties,
    }) => {
      setValuesForQuestion(
        {
          sightseeing,
          bigCities,
          beachResorts,
          waterActivities,
          restaurants,
          parties,
        },
        1
      )

      nextQuestion()
    },
  })

  const handleSubmit = React.useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault()
      formik.handleSubmit(e)
    },
    [formik]
  )

  return (
    <Question1
      {...props}
      values={formik.values}
      handleChange={formik.handleChange}
      onSubmit={handleSubmit}
      nextQuestion={nextQuestion}
    />
  )
}

export default Question1Container
