import React from 'react'
import { Button } from '../../../atoms'

type Props = React.HTMLProps<HTMLFormElement> & {
  prevQuestion: () => void
}

const Question2: React.FC<Props> = (props) => {
  return (
    <form {...props}>
      <Button
        className="mt-2"
        type="submit"
        caption="Back"
        variant="outlined"
      />
    </form>
  )
}

export default Question2
