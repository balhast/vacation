import React from 'react'
import { QuestionsContext } from '../../../../context'
import Question2 from './Question2'

const Question2Container: React.FC = () => {
  const { prevQuestion } = React.useContext(QuestionsContext)

  return <Question2 prevQuestion={prevQuestion} />
}

export default Question2Container
