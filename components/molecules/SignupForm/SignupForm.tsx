import React from 'react'
import { Button, Input } from '../../atoms'

type Props = React.HTMLProps<HTMLFormElement> & {
  handleChange: (e: React.FormEvent<HTMLInputElement>) => void
}

const SignupForm: React.FC<Props> = ({ handleChange, ...rest }) => {
  return (
    <form {...rest} className="max-w-md">
      <div className="mt-3 container mx-auto flex justify-center flex-wrap">
        <Input
          className="w-48 mr-2 mb-3"
          placeholder="First name"
          name="firstname"
          onChange={handleChange}
        />
        <Input
          className="w-48 mb-3"
          placeholder="Surname"
          name="surname"
          onChange={handleChange}
        />
        <Input
          className="w-98 mb-3"
          placeholder="Enter your email address"
          name="email"
          onChange={handleChange}
        />
        <Input
          className="w-98 mb-3"
          placeholder="Enter a new password"
          type="password"
          name="password"
          onChange={handleChange}
        />
        <Input
          className="w-48 mr-2 mb-8"
          placeholder="Country"
          name="country"
          onChange={handleChange}
        />
        <Input
          className="w-48 mb-8"
          placeholder="City"
          name="city"
          onChange={handleChange}
        />
        <Button caption="Next" variant="outlined" />
      </div>
    </form>
  )
}

export default SignupForm
