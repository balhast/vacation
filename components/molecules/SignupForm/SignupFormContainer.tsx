import { useFormik } from 'formik'
import { useRouter } from 'next/dist/client/router'
import React from 'react'
import SignupForm from './SignupForm'

type Props = React.HTMLProps<HTMLFormElement>

const SignupFormContainer: React.FC<Props> = (props) => {
  const router = useRouter()

  const formik = useFormik({
    initialValues: {
      firstname: '',
      surname: '',
      email: '',
      password: '',
      country: '',
      city: '',
    },
    onSubmit: ({ firstname, surname, email, password, country, city }) => {
      console.log({ firstname, surname, email, password, country, city })
    },
  })

  const handleSubmit = React.useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault
      formik.handleSubmit(e)
      router.push('/questions')
    },
    [formik]
  )

  return (
    <SignupForm
      {...props}
      handleChange={formik.handleChange}
      onSubmit={handleSubmit}
    />
  )
}

export default SignupFormContainer
